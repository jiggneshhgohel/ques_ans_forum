class Question < ActiveRecord::Base
  
  belongs_to :author, foreign_key: :user_id, class_name: 'User'
  
  has_many :answers, dependent: :destroy
  
end

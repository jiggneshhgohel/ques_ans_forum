class User < ActiveRecord::Base
	has_one :profile, dependent: :destroy
	has_many :questions, source: :author
	has_many :answers
end


